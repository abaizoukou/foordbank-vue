import VueRouter from 'vue-router';
import Donation from './components/Donation.vue';
import DonationList from './components/DonationList.vue';
import DonationForm from './components/DonationForm.vue';

const router = new VueRouter({
  routes: [
    { path: '/', component: DonationList },
    { path: '/donate', component: DonationForm },
    { path: '/:id', component: Donation },
    {
      path: '*',
      component: { template: '<h3 style="margin: 6rem">Not Found!</h3>' }
    }
  ],
  mode: 'history'
});

export default router;
